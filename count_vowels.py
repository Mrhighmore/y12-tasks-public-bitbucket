'''
TODO:

Create a program that will count the number of vowels in a sentence

pseudocode:

sentence <-- "this is my sentence with vowels in it usually."
vowel_count <--- 0
FOR letter in sentence:
    IF letter = "a':
        vowel_count +1
    ????????
    
OUTPUT the amount of vowels and the sentence.

Please finish the pseudocode  and then code it in python.
Test it to prove that it works.

Extension:

turn it into a function so the sentence can be sent as an argument.
also make a list of the vowels found
make the code deal with capital letters 
save the results of the vowel check and the sentence to a text file.

is there a more efficient way of finding the vowels without multiple IF , ELSE IF and ELSE statements?

'''